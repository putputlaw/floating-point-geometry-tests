# References

- [elm-geometry's LineSegment2d implementation](https://github.com/ianmackenzie/elm-geometry/blob/master/src/LineSegment2d.elm)
- [elm-units' Quantity implementation](https://github.com/ianmackenzie/elm-units/blob/2.7.0/src/Quantity.elm)
- [test: shared endpoint on third segment induces an intersection](https://github.com/ianmackenzie/elm-geometry/blob/73dfe4a8514f868d5d89559348342b2bfb433090/tests/Tests/LineSegment2d.elm#L313)
- [discussion between ianmackenzie and mpizenberg about line segment intersection](https://github.com/ianmackenzie/elm-geometry/pull/17)


# Output

Running the floating point number properties experiment

    $ cargo run --release
       Compiling floaty v0.1.0 (/tmp/floaty)
        Finished release [optimized] target(s) in 0.97s
         Running `target/release/floaty`
    test_inv_inv	fails:     1715421 / 10000000
    test_add_sub	fails:     7500637 / 10000000
    test_sub_add	fails:     3747246 / 10000000
    test_refl	fails:           0 / 10000000
    test_div_mul	fails:      998291 / 10000000

Runing the line segment tests (32bit floats with floating point equality as equality check)

    $ cargo test --release
       Compiling floaty v0.1.0 (/tmp/floaty)
        Finished release [optimized] target(s) in 15.88s
         Running /tmp/floaty/target/release/deps/floaty-7c7d70e3a806ff5d

    running 4 tests
    test test ... ok
    test test3 ... FAILED
    test test2 ... FAILED
    test test4 ... FAILED

    failures:

    ---- test3 stdout ----
    thread 'test3' panicked at 'assertion failed: `(left == right)`
      left: `12551`,
     right: `0`', src/main.rs:189:5
    note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

    ---- test2 stdout ----
    thread 'test2' panicked at 'assertion failed: `(left == right)`
      left: `73582`,
     right: `0`', src/main.rs:167:5

    ---- test4 stdout ----
    thread 'test4' panicked at 'assertion failed: `(left == right)`
      left: `9631`,
     right: `0`', src/main.rs:221:5

