use rand::{thread_rng, Rng};

// type Float = f64;
type Float = f32;
type Vec2 = vec2::Vec2<Float>;

fn eq(a: Float, b: Float) -> bool {
    a == b
    // (a - b).abs() <= 1e-6
    // (a - b).abs() <= 1e-15
}

fn vec_eq(v: Vec2, w: Vec2) -> bool {
    eq(v.x, w.x) && eq(v.y, w.y)
}

macro_rules! report {
    ($t: ident, $n: expr) => {
        let num_fails = run($t, $n);
        println!(
            "{}\tfails:    {:8} / {}",
            std::stringify!($t),
            num_fails,
            $n
        );
    };
}

fn main() {
    let num_iter = 10_000_000;
    report!(test_inv_inv, num_iter);
    report!(test_add_sub, num_iter);
    report!(test_sub_add, num_iter);
    report!(test_refl, num_iter);
    report!(test_div_mul, num_iter);
}

fn run(f: impl Fn(Float) -> bool, num_iter: usize) -> usize {
    let mut rng = thread_rng();
    let d = rand::distributions::Uniform::new_inclusive(0.0, 1.0);
    (&mut rng)
        .sample_iter(&d)
        .take(num_iter)
        .filter(|x| !f(*x))
        .count()
}

fn test_inv_inv(x: Float) -> bool {
    eq(1.0 / (1.0 / x), x)
}

fn test_add_sub(x: Float) -> bool {
    eq((x + 1.0) - 1.0, x)
}

fn test_sub_add(x: Float) -> bool {
    eq((x - 1.0) + 1.0, x)
}

fn test_refl(x: Float) -> bool {
    eq(x, x)
}

fn test_div_mul(x: Float) -> bool {
    eq((x / 5.0) * 5.0, x)
}

#[derive(Clone, Copy, Debug)]
pub struct Seg(Vec2, Vec2);

impl Seg {
    pub fn intersection(self, other: Self) -> Option<Vec2> {
        let Self(p, p_) = self;
        let Self(q, q_) = other;
        let r = p_ - p;
        let s = q_ - q;
        let pq = q - p;
        let pq_ = q_ - p;
        let qp_ = p_ - q;
        let pq_x_r = pq * r;
        let pq_x_s = pq * s;
        let s_x_qp_ = s * qp_;
        let r_x_pq_ = r * pq_;
        let t_denominator = pq_x_s - s_x_qp_;
        let u_denominator = pq_x_r + r_x_pq_;

        if eq(t_denominator, 0.0) || eq(u_denominator, 0.0) {
            if r.dot(s) < 0.0 {
                if vec_eq(p, q_) {
                    Some(p_)
                } else if vec_eq(p, q) {
                    Some(p)
                } else {
                    None
                }
            } else if vec_eq(p_, q) {
                Some(p_)
            } else if vec_eq(p, q_) {
                Some(p)
            } else {
                None
            }
        } else {
            let t = pq_x_s / t_denominator;
            let u = pq_x_r / u_denominator;

            if (0.0..=1.0).contains(&t) && (0.0..=1.0).contains(&u) {
                Some(if t.min(1.0 - t) <= u.min(1.0 - u) {
                    p.interpolate(p_, t)
                } else {
                    q.interpolate(q_, u)
                })
            } else {
                None
            }
        }
    }

    pub fn contains(self, o: Vec2) -> bool {
        let Self(p, p_) = self;
        let online = eq((p_ - p) * (o - p), 0.0);
        // let online  = ((p_ - p) * (o - p)).abs() < 1e-6;
        // let online  = ((p_ - p) * (o - p)).abs() < 1e-15;
        let inside1 = (p_ - p).dot(o - p) >= 0.0;
        let inside2 = (p_ - p).dot(o - p_) <= 0.0;
        online && inside1 && inside2
    }
}

#[test]
fn test() {
    let a = Vec2::new(0.0, 0.0);
    let b = Vec2::new(1.0, 0.0);
    let c = Vec2::new(1.0, 1.0);
    let d = Vec2::new(0.0, 1.0);
    let ab = Seg(a, b);
    let bc = Seg(b, c);
    let cd = Seg(c, d);
    let ac = Seg(a, c);
    let bd = Seg(b, d);
    assert_eq!(ab.intersection(bc), Some(b));
    assert_eq!(ac.intersection(bd), Some(Vec2::new(0.5, 0.5)));
    assert_eq!(ab.intersection(cd), None);
    assert_eq!(ab.intersection(ab), None);
}

#[test]
fn test2() {
    let mut rng = rand::thread_rng();
    let dist = rand::distributions::Uniform::new_inclusive(-0.5, 0.5);
    let unit_interval_dist = rand::distributions::Uniform::new_inclusive(0.0, 1.0);
    let num_iter = 100_000;

    let num_fails = (0..num_iter)
        .filter(|_| {
            let rs: Vec<Float> = (&mut rng).sample_iter(&dist).take(4).collect();
            let a = Vec2::new(rs[0], rs[1]);
            let b = Vec2::new(rs[2], rs[3]);
            let t = (&mut rng).sample(&unit_interval_dist);
            assert_eq!((true, t), (0.0 <= t && t <= 1.0, t));
            let c = a.interpolate(b, t);

            !Seg(a, b).contains(c)
        })
        .count();

    assert_eq!(num_fails, 0);
}

// like test 2, but with fixed t = 0.5
#[test]
fn test3() {
    let mut rng = rand::thread_rng();
    let dist = rand::distributions::Uniform::new_inclusive(-0.5, 0.5);
    let num_iter = 100_000;

    let num_fails = (0..num_iter)
        .filter(|_| {
            let rs: Vec<Float> = (&mut rng).sample_iter(&dist).take(4).collect();
            let a = Vec2::new(rs[0], rs[1]);
            let b = Vec2::new(rs[2], rs[3]);
            let t = 0.5;
            let c = a.interpolate(b, t);

            !Seg(a, b).contains(c)
        })
        .count();

    assert_eq!(num_fails, 0);
}


// A segment [a,b] with halfway point h is intersected by
// another segment [c,h] resulting in an intersection Some(h2) or None.
#[test]
fn test4() {
    let mut rng = rand::thread_rng();
    let dist = rand::distributions::Uniform::new_inclusive(-0.5, 0.5);
    let num_iter = 100_000;

    let num_fails = (0..num_iter)
        .filter(|_| {
            let rs: Vec<Float> = (&mut rng).sample_iter(&dist).take(6).collect();
            let a = Vec2::new(rs[0], rs[1]);
            let b = Vec2::new(rs[2], rs[3]);
            let c = Vec2::new(rs[4], rs[5]);
            let t = 0.5;
            let h = a.interpolate(b, t);
            let seg1 = Seg(a, b);
            let seg2 = Seg(h, c);
            let h2 = seg2.intersection(seg1);

            if let Some(h2) = h2 {
                !vec_eq(h2, h)
            } else {
                true
            }
        })
        .count();

    assert_eq!(num_fails, 0);
}

mod vec2 {
    use std::ops::{Add, Mul, Sub};

    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct Vec2<T> {
        pub x: T,
        pub y: T,
    }

    impl<T> Vec2<T> {
        pub fn new(x: T, y: T) -> Self {
            Self { x, y }
        }
    }

    impl<T> Add<Vec2<T>> for Vec2<T>
    where
        T: Add<Output = T>,
    {
        type Output = Self;

        fn add(self, other: Self) -> Self {
            Self {
                x: self.x + other.x,
                y: self.y + other.y,
            }
        }
    }

    impl<T> Sub<Vec2<T>> for Vec2<T>
    where
        T: Sub<Output = T>,
    {
        type Output = Self;

        fn sub(self, other: Self) -> Self {
            Self {
                x: self.x - other.x,
                y: self.y - other.y,
            }
        }
    }

    // Crossproduct
    impl<T> Mul<Vec2<T>> for Vec2<T>
    where
        T: Mul<Output = T> + Sub<Output = T> + Add<Output = T>,
    {
        type Output = T;

        fn mul(self, other: Self) -> T {
            self.x * other.y - self.y * other.x
        }
    }

    // dot product
    impl<T> Vec2<T>
    where
        T: Mul<Output = T> + Add<Output = T>,
    {
        pub fn dot(self, other: Self) -> T {
            self.x * other.x + self.y * other.y
        }
    }

    impl<T> Vec2<T>
    where
        T: Mul<Output = T> + Add<Output = T> + Sub<Output = T> + One + Copy,
    {
        pub fn interpolate(self, other: Self, t: T) -> Self {
            Self {
                x: (T::one() - t) * self.x + t * other.x,
                y: (T::one() - t) * self.y + t * other.y,
            }
        }
    }

    pub trait One {
        fn one() -> Self;
    }

    impl One for f32 {
        fn one() -> Self {
            1.0f32
        }
    }

    impl One for f64 {
        fn one() -> Self {
            1.0f64
        }
    }
}
